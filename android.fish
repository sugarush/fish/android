set -l ANDROID_HOME ~/Library/Android/sdk

if ! contains $ANDROID_HOME/emulator $PATH
  set -a PATH $ANDROID_HOME/emulator
end

if ! contains $ANDROID_HOME/platform-tools $PATH
  set -a PATH $ANDROID_HOME/platform-tools
end
